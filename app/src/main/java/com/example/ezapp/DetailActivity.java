package com.example.ezapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.ezapp.Model.Seance;
import com.example.ezapp.Model.SeanceTime;
import com.google.android.material.internal.FlowLayout;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.details);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            // setting poster
            ImageView posterView = findViewById(R.id.detail_poster);
            if (posterView != null) {
                String posterURL = extras.getString("EXTRA_PERFORMANCE_IMAGE");
                Picasso.with(getBaseContext())
                        .load(posterURL)
                        .into(posterView);
            }

            // setting play button
            ImageView playPosterView = findViewById(R.id.detail_poster_play);
            if (playPosterView != null) {
                final String trailerURL = extras.getString("EXTRA_PERFORMANCE_TRAILER");

                if (trailerURL != null) {
                    playPosterView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(trailerURL));
                            startActivity(intent);
                        }
                    });
                } else {
                    playPosterView.setVisibility(View.INVISIBLE);
                }
            }

            // setting title
            String title = extras.getString("EXTRA_PERFORMANCE_TITLE");
            TextView titleView = findViewById(R.id.detail_title);
            if (titleView != null) {
                titleView.setText(title);
            }

            // Setting press stars
            RatingBar pressRating = findViewById(R.id.detail_press_rating);
            if (pressRating != null) {
                String rating = extras.getString("EXTRA_PERFORMANCE_PRESS_RATING");
                if (rating != null) {
                    Double value = Double.valueOf(rating);
                    pressRating.setRating(value.floatValue());
                }
            }

            // Setting press text rating
            TextView pressTextRating = findViewById(R.id.detail_press_text_rating);
            if (pressTextRating != null) {
                pressTextRating.setText(extras.getString("EXTRA_PERFORMANCE_PRESS_VALUE"));
            }

            // Setting user stars
            RatingBar userRating = findViewById(R.id.detail_user_rating);
            if (userRating != null) {
                String rating = extras.getString("EXTRA_PERFORMANCE_USER_RATING");
                if (rating != null) {
                    Double value = Double.valueOf(rating);
                    userRating.setRating(value.floatValue());
                }
            }

            // Setting user text rating
            TextView userTextRating = findViewById(R.id.detail_user_text_rating);
            if (userTextRating != null) {
                userTextRating.setText(extras.getString("EXTRA_PERFORMANCE_USER_VALUE"));
            }

            // Setting genre info
            TextView genreText = findViewById(R.id.detail_genre);
            if (genreText != null) {
                genreText.setText(extras.getString("EXTRA_PERFORMANCE_GENRES"));
            }

            // Setting display info
            // TODO : list of Seances in FlowLayout
            LinearLayout seancesLayout = findViewById(R.id.detail_seances_layout);
            if (seancesLayout != null) {
                String json = extras.getString("EXTRA_PERFORMANCE_SEANCES");
                Seance[] seances = new Gson().fromJson(json, Seance[].class);

                for (Seance seance : seances) {
                    LinearLayout linearLayout = new LinearLayout(getBaseContext());
                    linearLayout.setOrientation(LinearLayout.VERTICAL);

                    TextView seanceDate = new TextView(getBaseContext());
                    seanceDate.setText(seance.getDate().toString());
                    linearLayout.addView(seanceDate);

                    for (final SeanceTime seanceTime : seance.getSeanceTimes()) {
                        Button seanceTimeButton = new Button(getBaseContext());
                        seanceTimeButton.setText(seanceTime.getName());
                        seanceTimeButton.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                Intent browserIntent = new Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse(seanceTime.getURL()));

                                startActivity(browserIntent);
                            }
                        });

                        linearLayout.addView(seanceTimeButton);
                        linearLayout.setTop(1);
                    }

                    seancesLayout.addView(linearLayout);
                }
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit:
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
