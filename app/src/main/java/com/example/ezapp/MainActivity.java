package com.example.ezapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.util.Log;

import com.example.ezapp.Model.MovieCalendar;
import com.example.ezapp.Model.Performance;
import com.example.ezapp.Services.APIClient;
import com.example.ezapp.Widgets.PerformanceAdapter;

public class MainActivity extends AppCompatActivity {
    static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final MainActivity cpy = this;

        setTitle("EZCorp - Ciné dôme");

        APIClient.getInstance().getMoviesApi().getMovieCalendar().enqueue(new Callback<MovieCalendar>() {
            @Override
            public void onResponse(Call<MovieCalendar> call, Response<MovieCalendar> response) {
                MovieCalendar calendar = response.body();

                if(calendar != null) {

                    RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list);

                    // use this setting to improve performance if you know that changes
                    // in content do not change the layout size of the RecyclerView
                    mRecyclerView.setHasFixedSize(true);

                    // use a linear layout manager
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(cpy);
                    mRecyclerView.setLayoutManager(mLayoutManager);

                    // specify an adapter (see also next example)
                    PerformanceAdapter mAdapter = new PerformanceAdapter(calendar.getPerformances());
                    mRecyclerView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<MovieCalendar> call, Throwable t) {
                Log.e(TAG, "failure : " + t.getMessage());
                t.printStackTrace();
            }
        });
    }
}
