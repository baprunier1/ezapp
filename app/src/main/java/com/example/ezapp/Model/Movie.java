package com.example.ezapp.Model;

public class Movie {
    String title;
    Genre[] genre;
    Link poster;
    Statistics statistics;
    Trailer trailer;

    public String getTitle(){
        return title;
    }

    public Link getPosterLink(){
        return poster;
    }

    public Trailer getTrailer(){
        return trailer;
    }

    public Statistics getStatistics() {
        return statistics;
    }

    public Genre[] getGenre(){
        return genre;
    }
}
