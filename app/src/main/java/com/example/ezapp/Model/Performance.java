package com.example.ezapp.Model;

public class Performance {
    ShowInfo onShow;
    String display;
    Seance[] scr;

    public String getDisplay(){
        return display;
    }

    public String toString(){
        return display;
    }

    public ShowInfo getShowInfo() {
        return onShow;
    }

    public Seance[] getSeances(){
        return scr;
    }
}
