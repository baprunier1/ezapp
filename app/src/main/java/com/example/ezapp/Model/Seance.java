package com.example.ezapp.Model;

import java.util.Date;

public class Seance {
    private Date d;
    private SeanceTime[] t;

    public Date getDate() {
        return d;
    }

    public SeanceTime[] getSeanceTimes() {
        return t;
    }
}
