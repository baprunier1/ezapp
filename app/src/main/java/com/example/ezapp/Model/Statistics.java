package com.example.ezapp.Model;

public class Statistics {
    Double pressRating;
    Integer pressReviewCount;
    Double userRating;
    Integer userReviewCount;
    Integer userRatingCount;

    public Double getPressRating() {
        return pressRating;
    }

    public Integer getPressReviewCount() {
        return pressReviewCount;
    }

    public Double getUserRating() {
        return userRating;
    }

    public Integer getUserReviewCount() {
        return userReviewCount;
    }

    public Integer getUserRatingCount() {
        return userRatingCount;
    }
}
