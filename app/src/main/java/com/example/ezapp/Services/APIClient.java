package com.example.ezapp.Services;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private MoviesApi moviesApi;

    public MoviesApi getMoviesApi() {
        return moviesApi;
    }

    private APIClient() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://etudiants.openium.fr/pam/").addConverterFactory(GsonConverterFactory.create()).build();
        moviesApi = retrofit.create(MoviesApi.class);
    }

    private static volatile APIClient instance;


    public static synchronized APIClient getInstance() {
        if (instance == null) {
            instance = new APIClient();
        }
        return instance;
    }
}
