package com.example.ezapp.Services;

import com.example.ezapp.Model.MovieCalendar;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MoviesApi {

    /**
     * Call a webservice with dynamic path
     * Exemple :
     * "https://api.myjson.com/bins/31245"
     * base url : https://api.myjson.com/
     * path : bins/{id}
     * where id = 31245
     * So the method will be called using getNews("31235");
     */

    @GET("cine.json")
    Call<MovieCalendar> getMovieCalendar();
}
