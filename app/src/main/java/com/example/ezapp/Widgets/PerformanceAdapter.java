package com.example.ezapp.Widgets;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ezapp.DetailActivity;
import com.example.ezapp.Model.Genre;
import com.example.ezapp.Model.Performance;
import com.example.ezapp.Model.Seance;
import com.example.ezapp.Model.Statistics;
import com.example.ezapp.R;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import androidx.recyclerview.widget.RecyclerView;

public class PerformanceAdapter extends RecyclerView.Adapter<PerformanceAdapter.MyViewHolder> {
    private Performance[] mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public RelativeLayout mRelativeLayout;
        public TextView mTextView;
        public ImageView mImageView;
        public Context mContext;
        public Performance performance;

        public MyViewHolder(RelativeLayout v, Context context) {
            super(v);
            mRelativeLayout = v;
            mTextView = v.findViewById(R.id.title);
            mImageView = v.findViewById(R.id.poster);
            mContext = context;

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, DetailActivity.class);
            if (performance.getShowInfo().getMovie().getTrailer() != null) {
                intent.putExtra("EXTRA_PERFORMANCE_TRAILER", performance.getShowInfo().getMovie().getTrailer().getURL());
            }
            intent.putExtra("EXTRA_PERFORMANCE_IMAGE", performance.getShowInfo().getMovie().getPosterLink().getURL());
            intent.putExtra("EXTRA_PERFORMANCE_TITLE", performance.getShowInfo().getMovie().getTitle());

            // statistics
            Statistics statistics = performance.getShowInfo().getMovie().getStatistics();
            if (statistics != null) {
                Double pressRating = statistics.getPressRating();
                if (pressRating != null) {
                    pressRating = (double) Math.round(pressRating * 10) / 10;
                    intent.putExtra("EXTRA_PERFORMANCE_PRESS_RATING", pressRating.toString());
                    intent.putExtra("EXTRA_PERFORMANCE_PRESS_VALUE", pressRating.toString() + "/5 (" + statistics.getPressReviewCount().toString() + " reviews)");
                }

                Double userRating = statistics.getUserRating();
                if (userRating != null) {
                    userRating = (double) Math.round(userRating * 10) / 10;
                    intent.putExtra("EXTRA_PERFORMANCE_USER_RATING", userRating.toString());
                    intent.putExtra("EXTRA_PERFORMANCE_USER_VALUE", userRating.toString() + "/5 (" + statistics.getUserRatingCount().toString() + " votes)");
                }
            }

            // genres
            Genre[] genres = performance.getShowInfo().getMovie().getGenre();
            if (genres != null && genres.length > 0) {
                StringBuilder genresStr = new StringBuilder("Genres : ");
                for (int i = 0; i < genres.length - 1; ++i) {
                    genresStr.append(genres[i].getName()).append(", ");
                }
                genresStr.append(genres[genres.length - 1].getName());
                intent.putExtra("EXTRA_PERFORMANCE_GENRES", genresStr.toString());
            }

            Seance[] seances = performance.getSeances();
            if (seances != null && seances.length > 0) {
                String json = new Gson().toJson(seances);
                intent.putExtra("EXTRA_PERFORMANCE_SEANCES", json);
            }

            // start activity
            mContext.startActivity(intent);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PerformanceAdapter(Performance[] myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PerformanceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // getting Relative layout
        RelativeLayout l = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_movie, parent, false);

        // create a new view
        MyViewHolder vh = new MyViewHolder(l, parent.getContext());
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.setText(mDataset[position].getShowInfo().getMovie().getTitle());
        holder.performance = mDataset[position];

        // press rating
        RatingBar pressRating = holder.mRelativeLayout.findViewById(R.id.cell_press_rating);
        if (pressRating != null) {
            Double value = holder.performance.getShowInfo().getMovie().getStatistics().getPressRating();
            if (value != null) {
                pressRating.setRating(value.floatValue());
            } else {
                pressRating.setRating(0);
            }
        }

        // user rating
        RatingBar userRating = holder.mRelativeLayout.findViewById(R.id.cell_users_rating);
        if (userRating != null) {
            Double value = holder.performance.getShowInfo().getMovie().getStatistics().getUserRating();
            if (value != null) {
                userRating.setRating(value.floatValue());
            } else {
                userRating.setRating(0);
            }
        }

        Picasso.with(holder.mContext)
                .load(mDataset[position].getShowInfo().getMovie().getPosterLink().getURL())
                .into(holder.mImageView);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
